const morgan = require('morgan')
const MongoClient = require('mongodb').MongoClient
const { findParticipantByEmail, findWorkshopByDate, bookWorkshopSession, updateWorkshopSession } = require('./database') 
const express = require('express')

const MAX_PER_SESSION = 20
const MONGO_URL = process.env['MONGO_URL'] || 'mongodb://localhost:27017/fsd2020'

const sendError = (code, errObj, resp) =>
	resp.status(code).type('application/json')
		.json({ error: errObj })

const sendResult = (code, result, resp) =>
	resp.status(code).type('application/json')
		.json(result)


const idx = MONGO_URL.indexOf(':')
console.info(`**** Using ${MONGO_URL.substring(0, idx)} ****`)

const app = express()

app.use(morgan('combined'))

app.get('/api/session/:date/count', (req, resp) => {
	const { date } = req.params

	const client = new MongoClient(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true })

	client.connect()
		.then(client => findWorkshopByDate(date, client))
		.then(result => {
			const amCount = result.filter(v => 'am' == v['session']).length
			resp.status(200).type('application/json')
				.json({ am: amCount, pm: result.length - amCount, max: MAX_PER_SESSION, date })
		})
		.catch(error => sendError(500, error, resp))
		.finally(() => {
			try { client.close() } catch(e) { }
		})
})

app.post('/api/session', express.json(), (req, resp) => {
	const { email, date, session } = req.body
	const client = new MongoClient(MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true })
	client.connect()
		.then(client => findParticipantByEmail(email, client))
		.then(part => {
			if (!part) {
				sendError(404, `Cannot find email: ${email}`, resp)
				return false
			}
			return findWorkshopByDate(date, client)
		})
		.then(result => {
			if (!result)
				return
			const count = result.filter(v => session == v['session'])
			if (count >= MAX_PER_SESSION) {
				sendError(400, `${session.toUpperCase()} on ${date} is full.`, resp) 
				return  false
			}
			const rec = result.find(v => email == v['email'])
			if (!rec)
				return bookWorkshopSession({email, date, session}, client)
			rec['session'] = session
			return updateWorkshopSession(rec, client)
		})
		.then(result => {
			if (!result)
				return
			sendResult(200, {ok: result.result}, resp)
		})
		.catch(error => {
			return sendError(500, error, resp)
		})
		.finally(() => {
			try { client.close() } catch(e) { }
		})
	}
)

module.exports = app
