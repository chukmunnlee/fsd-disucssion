const mongodb = require('mongodb')

const DATABASE = 'fsd2020'
const PARTICIPANTS = 'participants'
const SLOTS = 'slots'
const WORKSHOP = 'workshops'

const findParticipantByEmail = (email, client) => 
	client.db(DATABASE).collection(PARTICIPANTS)
		.findOne({ email })

const findSlotByEmail = (email, client) => 
	client.db(DATABASE).collection(SLOTS)
		.findOne({ email })

const findSlotById = (id, client) => {
	const _id = mongodb.ObjectId(id)
	return client.db(DATABASE).collection(SLOTS)
		.findOne({ _id })
}

const findWorkshopByDate = (date, client) => {
	return client.db(DATABASE).collection(WORKSHOP)
		.find({ date })
		.toArray()
}

const bookWorkshopSession = (obj, client) => {
	return client.db(DATABASE).collection(WORKSHOP)
		.insertOne({ email: obj['email'], date: obj['date'], session: obj['session'] })
}

const updateWorkshopSession = (obj, client) => {
	const updateDoc = {
		email: obj['email'],
		date: obj['date'],
		session: obj['session'],
	}
	return client.db(DATABASE).collection(WORKSHOP)
		.updateOne({ _id: obj['_id'] }, 
			{ $set: { session: obj['session'] } },
			{ upsert: true }
		)
}

const updateBookings = (booked, client) => {
	const updateDoc = { 
		email: booked.email, group: booked.group, password: booked.password,
		day: booked.updateDay, slot: booked.updateSlot, track: booked.updateTrack, 
		meet: booked.meet
	}

	if (-1 == booked.day) 
		return client.db(DATABASE).collection(SLOTS)
			.insertOne(updateDoc);

	const filter = {
		email: booked.email, group: booked.group, password: booked.password,
		day: booked.day, slot: booked.slot, track: booked.track
	}

	return client.db(DATABASE).collection(SLOTS)
		.update(
			filter, updateDoc,
			{ upsert: true }
		)
}

const findBookedSlotsByGroup = (group, client) => 
	client.db(DATABASE).collection(SLOTS)
		.find({ group, email: { $exists: true } })
		.project({ _id: 0, email: 1, day: 1, slot: 1, track: 1 })
		.toArray()

module.exports = { 
	findParticipantByEmail, 
	findSlotByEmail, findSlotById, 
	findBookedSlotsByGroup,
	updateBookings,
	findWorkshopByDate, bookWorkshopSession, updateWorkshopSession
}

