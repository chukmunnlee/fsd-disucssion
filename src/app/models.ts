export interface SessionCount {
	am: number
	pm: number
	date: string
	max: number
}

export interface BookSession {
	session: string //am or pm
	email: string
	date: string
}

