import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import {DiscussionService} from './discussion.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, HttpClientModule,
	 FormsModule, ReactiveFormsModule
  ],
  providers: [ DiscussionService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
