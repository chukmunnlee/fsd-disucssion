import {HttpClient} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {BookSession, SessionCount} from "./models";

@Injectable()
export class DiscussionService {
	constructor(private http: HttpClient) { }

	// date format month-day-2021 eg apr-16-2021
	getSessionCount(date: string): Promise<SessionCount> {
		return this.http.get<SessionCount>(`/api/session/${date}/count`)
			.toPromise()
	}

	bookSession(email: string, date: string, session: string): Promise<any> {
		const payload: BookSession = { email, date, session }
		return this.http.post<any>('/api/session/', payload)
			.toPromise()
	}
}
