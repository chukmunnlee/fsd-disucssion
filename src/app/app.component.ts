import {HttpErrorResponse} from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DiscussionService} from './discussion.service';

const toString = thingy => {
	if ('object' == typeof(thingy))
		return JSON.stringify(thingy)
	return thingy
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

	form: FormGroup
	session = { am: 0, pm: 0 }
	maxPerSession = 0
	amLabel = '0/0'
	pmLabel = '0/0'
	feedbackMsg = ''
	isError = false
	morning = false

	constructor(private fb: FormBuilder, private discussSvc: DiscussionService) { }

	ngOnInit() {
		this.form = this.fb.group({
			date: this.fb.control("", [ Validators.required ]),
			email: this.fb.control("", [ Validators.required, Validators.email ])
		})
	}

	onDateChange() {
		const date = this.form.value['date']
		this.discussSvc.getSessionCount(date)
			.then(result => {
				this.maxPerSession = result.max
				this.session = { am: result.am, pm: result.pm }
				this.amLabel = `${result.am}/${this.maxPerSession}`
				this.pmLabel = `${result.pm}/${this.maxPerSession}`
			})
			.catch((errResp: HttpErrorResponse) => {
				this.feedbackMsg = toString(errResp.error.error)
				this.isError = true
			})
	}

	bookSession(sess: string) {
		const { email, date } = this.form.value
		this.feedbackMsg = ''
		this.isError = false
		this.morning = sess == 'am'
		this.discussSvc.bookSession(email, date, sess)
			.then(result => {
				this.reset()
				if ('nModified' in result['ok'])
					this.feedbackMsg = `You have update your session to ${date.toUpperCase()}/${sess.toUpperCase()}`
				else
					this.feedbackMsg = `You have booked ${date.toUpperCase()}/${sess.toUpperCase()} session.`
			})
			.catch((errResp: HttpErrorResponse) => {
				this.feedbackMsg = toString(errResp.error.error)
				this.isError = true
			})
	}

	reset() {
		this.form.reset()
		this.session = { am: 0, pm: 0 }
		this.amLabel = '0/0'
		this.pmLabel = '0/0'
		this.maxPerSession = 0
	}

	shouldDisable(sess: string) {
		return this.form.invalid || (this.session[sess] >= this.maxPerSession)
	}
}
